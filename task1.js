const jsonfile = require('jsonfile');
const randomstring = require("randomstring");

const file1 = './input2.json';
const file2 = './output2.json'
jsonfile.readFile(file1, function (err, obj) {
  if (err) console.error(err);
  let fakeEmails = obj.names.map(reverseStr).map(generateFakeEmail);

  jsonfile.writeFile(file2, {email: fakeEmails}, function (err) {
    if (err) console.error(err);
  });
});

function reverseStr(input) {
    let output = "";

    for (let i = 0; i < input.length; i++) {
        output = input[i] + output;
    }

    return output;
}

function generateFakeEmail(name) {
    let randomStr = randomstring.generate(5);
    
    return name + randomStr + '@gmail';
}